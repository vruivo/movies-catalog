const fs = require('fs');
// const readline = require('readline');
const config = require('../config.json');
const movieProvider = require('libs/movie-provider')(config.providers.tmdb);
// const uiUtils = require('libs/ui-utils');

var myMovies = [];

const dataDir = fs.realpathSync("data") + "/";
const cacheDir = dataDir + "cache/";
const postersDir = dataDir + "posters/";

const moviesContainerEl = document.querySelector('#movies-container');
const addMovieContainerEl = document.querySelector('#add-movie-container');

function save(catalog) {
  catalog = catalog || "movies";
  var data;

  switch (catalog) {
    case "expression":
      break;
    case "movies":
    default:
      catalog = "movies";
      data = myMovies;
      break;
  }

  fs.writeFileSync(dataDir + catalog + ".json", JSON.stringify(data));
}

function load() {
  const filename = dataDir + "movies.json";
  fs.accessSync(filename, fs.R_OK | fs.W_OK);
  myMovies = require(filename);

  myMovies.forEach(function(val) { addMovieToGrid(val); });
}

function formatPosterFilename(title, releaseDate) {
  if (!title || !releaseDate) return null;
  if (typeof title != "string" ||
      typeof releaseDate != "string") return null;

  var filename = title + releaseDate;
  filename = filename.replace(/[^a-zA-Z0-9]/g,'_');
  return filename;
}

function addNewMovieToCatalog(id, successCB, errorCB) {
  movieProvider.getMovieDetails(id, function(data) {
    if (data.poster) {
      data.poster = formatPosterFilename(data.title, data.releaseDate);
    }
    data.dateAdded = Date.now();
    myMovies.push(data);

    const el = addMovieToGrid(data, "small");

    if (data.poster) {
      loadMoviePoster(postersDir + "/" + data.poster, data, el);
    }

    successCB();
  }, function() {
    console.log("error");
    errorCB();
  });
}

function addMovieToGrid(movie, poster_size) {
  poster_size = poster_size || "normal";
  var path;

  switch (poster_size) {
    case "small":
      path = cacheDir + movie.poster + "_small.jpg";
      break;
    case "normal":
    default:
      path = postersDir + movie.poster + ".jpg";
      break;
  }

  var posterImg = document.createElement("img");
  posterImg.setAttribute("src", path);
  posterImg.setAttribute("class", "poster");
  posterImg.setAttribute("alt", "Poster");
  posterImg.setAttribute("id", "id" + movie.imdbId);
  posterImg.setAttribute("title", movie.title +
      " (" + movie.releaseDate.substr(0,4) + ")");
  moviesContainerEl.appendChild(posterImg);

  return posterImg;
}

function addMovieToList(movie) {
  // console.log(data.results[i].title, data.results[i].release_date);
  var div = document.createElement("div");
  div.setAttribute("data-id", movie.id);
  // div.setAttribute("data-poster", data.results[i].poster_path);
  // div.setAttribute("data-idx", i);
  div.setAttribute("onclick", "htmlAddMovie(this)");
  div.setAttribute("class", "movie-list-item");
  // div.appendChild(document.createTextNode(data.results[i].title));

  var posterImg = document.createElement("img");
  posterImg.setAttribute("src", dataDir + "noimage_small.jpg");
  posterImg.setAttribute("class", "poster small");
  posterImg.setAttribute("alt", "Poster");
  div.appendChild(posterImg);

  var infoDiv = document.createElement("div");
  infoDiv.appendChild(document.createTextNode(movie.title));
  infoDiv.appendChild(document.createElement("br"));
  infoDiv.appendChild(document.createTextNode(movie.release_date));
  div.appendChild(infoDiv);
  addMovieContainerEl.appendChild(div);

  loadMoviePoster(cacheDir + "/" + formatPosterFilename(movie.title, movie.release_date),
      movie.poster_path, posterImg, "small");
}

function loadMoviePoster(filePath, url_or_obj, el, size) {
  // var filename = formatPosterFilename(movie.title, movie.release_date);
  // filename += "_small.jpg";
  // const filePath = destinationDir + "/" + filename;
  // const poster_url = movie.poster_path;
  size = size || "normal";
  switch (size) {
    case "small":
      filePath += "_small.jpg";
      break;
    case "normal":
    default:
      filePath += ".jpg";
      break;
  }

  if (!url_or_obj)
    return;

  // check if image already exists
  try {
    fs.accessSync(filePath, fs.R_OK);
    el.setAttribute('src', filePath);
    return;
  } catch (e) {
    // continue and download image
  }

  const conf = {
    url: url_or_obj,
    filePath: filePath,
    size: "small"
  };
  movieProvider.getMoviePoster(conf, function(filePath) {
    el.setAttribute('src', filePath);
  }, function(data) {
    console.log("error", data);
  });
}


module.exports = {
  save: save,
  load: load,
  addNewMovieToCatalog: addNewMovieToCatalog,
  addMovieToList: addMovieToList
};
