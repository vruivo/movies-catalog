'use strict';

// const fs = require('fs');
// const readline = require('readline');
const config = require('./config.json');
const movieProvider = require('libs/movie-provider')(config.providers.tmdb);
const uiUtils = require('libs/ui-utils');
const controller = require("./js/index-controller");

const searchBoxEl = document.querySelector('#search-box');
const moviesContainerEl = document.querySelector('#movies-container');
const addMovieContainerEl = document.querySelector('#add-movie-container');

// var myMovies = [];
// var movies_found = [];
var typingTimer;

searchBoxEl.addEventListener('keyup', function(ev) {
  var text = searchBoxEl.value;
  clearTimeout(typingTimer);
  if (ev.keyCode === 13)  // Enter key, immediate action
    searchBoxAction(text);
  else
    typingTimer = setTimeout(searchBoxAction.bind({},text), 500);
});


controller.load();


function searchBoxAction(filter) {
  uiUtils.showElement(moviesContainerEl);
  uiUtils.filterCollection(filter, moviesContainerEl, ".poster");

  // add movie button
  addMovieContainerEl.innerHTML = "";
  if (filter) {
    uiUtils.showElement(addMovieContainerEl);
    var div = document.createElement("div");
    // div.setAttribute("class", "movie-grid-item");
    div.appendChild(document.createTextNode("Add movie: " + filter));
    div.setAttribute("onclick", "htmlSearchForMovieInProvider()");
    addMovieContainerEl.appendChild(div);
  }
  else {
    uiUtils.hideElement(addMovieContainerEl);
  }
}


function htmlSearchForMovieInProvider() {
  movieProvider.searchForMovie(searchBoxEl.value, function(data) {
    // movies_found = data.results;

    uiUtils.hideElement(moviesContainerEl);

    addMovieContainerEl.innerHTML = "";
    // if (filter) {
    // }

    // create list with results
    var len = data.results.length;
    for (var i = 0; i < len; i++) {
      controller.addMovieToList(data.results[i]);
    }
  }, function(data) {
    console.log(data);
  });
}


function htmlAddMovie(el) {
  const id = el.getAttribute("data-id");
  controller.addNewMovieToCatalog(id, function() {
    searchBoxEl.value = "";
    uiUtils.keyup(searchBoxEl, 13);
    controller.save();
    console.log("saved");
  }, function() {
    console.log("error");
  });
}




// var file = '/media/sf_tmp_share/js-movies/MyMovies.txt';
//
// const rl = readline.createInterface({
//   input: fs.createReadStream(file)
// });
//
// rl.on('line', function(line) {
//   var obj = JSON.parse(line);
//   if (obj.name) {
//     // console.log(obj.name);
//     var div = document.createElement("div");
//     div.setAttribute("class", "movie-grid-item");
//     div.appendChild(document.createTextNode(obj.name));
//     moviesContainerEl.appendChild(div);
//     // movies.push(obj.name);
//   }
// });
