'use strict';

// var app = require('electron');
// var BrowserWindow = require('browser-window');
const electron = require('electron');
// Module to control application life.
const {app} = electron;
// Module to create native browser window.
const {BrowserWindow} = electron;

const fs = require('fs');

var mainWindow = null;

app.on('ready', function() {
  // create required directories
  function makeDirSync(path) {
    try {
      fs.mkdirSync(path);
    } catch (e) {
      if (e.code !== 'EEXIST') {
        console.log("Error creating required directories");
        process.exit(1);
      }
    }
  }
  makeDirSync("./data");
  makeDirSync("./data/cache");
  makeDirSync("./data/posters");

  mainWindow = new BrowserWindow({
    height: 600,
    width: 740
  });

  mainWindow.loadURL('file://' + __dirname + '/app/index.html');

    // Open the DevTools.
  // mainWindow.webContents.openDevTools();
});
